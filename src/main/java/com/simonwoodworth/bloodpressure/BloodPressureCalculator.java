/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simonwoodworth.bloodpressure;
public class BloodPressureCalculator {

    /**
     * Calculates the Mean Arterial Pressure (MAP).
     *
     * @param systolic  the systolic blood pressure
     * @param diastolic the diastolic blood pressure
     * @return the calculated mean arterial pressure
     */
    public static double calculateMAP(double systolic, double diastolic) {
    if (systolic < 0 || diastolic < 0) {
        throw new IllegalArgumentException("Systolic and Diastolic pressures must be non-negative.");
    }
    return diastolic + (systolic - diastolic) / 3.0;
}

/*
    public static void main(String[] args) {
        // Example usage:
        double systolicPressure = 120;   // replace with actual systolic pressure
        double diastolicPressure = 80;   // replace with actual diastolic pressure

        double meanArterialPressure = calculateMAP(systolicPressure, diastolicPressure);
        System.out.println("The Mean Arterial Pressure is: " + meanArterialPressure + " mmHg");
    }
*/
}
